from bubble_sort import __version__
from bubble_sort.bubble_sort import bubble_sort
import pytest


test_data = [
    ([3, 0, 1], [0, 1, 3]),
    ([5, 4, 1, 10], [1, 4, 5, 10]),
    ([3, 2, 1], [1,2,3]), 
    ([-12,23,1,0,-5,2], [-12, -5, 0, 1, 2, 23]),
    ([-99, 56, 34, 55, 21, 1], [-99, 1, 21, 34, 55, 56])
]

def test_version():
    assert __version__ == '0.1.0'


@pytest.mark.parametrize('test_input, expected_result', test_data)
def test_one_bubblesort(test_input, expected_result):
    assert bubble_sort(test_input) == expected_result


@pytest.mark.xfail
def test_two_bubble_sort():
    assert bubble_sort([1, 0, 2, 5, 12, -1]) == [-1, 0, 1, 2, 12, 5]
