import click
from bubble_sort.bubble_sort import bubble_sort


@click.group()
def main():
    pass


@click.command()
@click.option('--data', default=None, help='Числа для запуска сортировки')
def b_sort(data):
    data = data.split(',')
    print(bubble_sort.bubble_sort(data))


@click.command()
@click.option('--start', default=1)
@click.option('--step', default=22)
@click.option('--end', default=10)
@click.option('--count', default=5)
def generate_d(start: int, end: int, step: int, count: int):
    pass


@click.command()
@click.option()
def measure_fun():
    pass


@click.comand()
@click.option('--file')
def create_char():
    pass


if __name__ == '__main__':
    main()
