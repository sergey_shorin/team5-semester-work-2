import os
import csv
from time import time
from bubble_sort.bubble_sort.bubble_sort import bubble_sort
from heap_sort.heap_sort.heap_sort import heap_sort
from collections import defaultdict
import statistics


def measure_time(function):
    function_name = function.__name__
    try:
        os.mkdir(path='load_testing_measurements')
    
    except OSError:
        print('Path is also exists')
    
    finally:
        dict_of_size_time = defaultdict(list)
        test_files = os.listdir(path='load_testing_data/')
        name = time()

        with open(file=f'load_testing_measurements/{name}-{function_name}.csv', mode='w') as file_csv:
            writer = csv.writer(file_csv, delimiter=',')
            writer.writerow(['size', 'min', 'max', 'avg', 'median'])
            for file in sorted(test_files, key=lambda x: x[:-4]):
                with open(f'load_testing_data/{file}', 'r') as f:
                    size = file.split('_')[0]
                    data = [int(i) for i in f.read().split(' ')]
                    start_time = time()
                    function(data)
                    end_time = (time() - start_time) * 1000
                    dict_of_size_time[size].append(end_time)
            for size, time_values in dict_of_size_time.items():
                writer.writerow([size, min(time_values), max(time_values), statistics.mean(time_values), statistics.median(time_values)])
                

measure_time(bubble_sort)
measure_time(heap_sort)
